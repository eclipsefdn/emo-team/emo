# Eclipse Management Organization

This repository is used to track issues involving the Eclipse Management Organization (EMO), including matters pertaining to the implementation of the Eclipse Foundation Development Process (EDP), Eclipse Foundation Specification Process (EFSP), and Eclipse IP Policy.

The [Eclipse Foundation Project Handbook](https://www.eclipse.org/projects/handbook) is a primary source of information regarding the operation of Eclipse Foundation open source projects; it contains a lot of helpful reference information for project teams that are implementing the Eclipse Development Process.

Use this repository's issue tracker to create issues for:

- [Request a process change or clarification](https://gitlab.eclipse.org/eclipsefdn/iplab/emo/-/issues/new?issuable_template=process)
- [Request a review](https://gitlab.eclipse.org/eclipsefdn/iplab/emo/-/issues/new?issuable_template=review)

CVE tracking has been moved to the Security Team's issue tracker:

- [Request a CVE](https://gitlab.eclipse.org/security/cve-assignement/-/issues/new?issuable_template=cve)

For all other issues, especially those related to infrastructure (e.g., creation of new Git repositories, or problems with the build infrastructure) use [Help Desk](https://gitlab.eclipse.org/eclipsefdn/helpdesk).
