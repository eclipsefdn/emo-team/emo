<!--
This template is used by the EMO to track the termination of a project.

Parts of this issue can initially be left blank.

Set the title of this issue to the name of the project_name

You can delete the comments (or not).
-->

The EMO is using this issue to track the progress of a project termination review. 

We have cited lack of development activity as the reason for the termination. Project leads and committers can add more to the review documentation if desired.

The purpose of a Termination Review is to provide a final opportunity for the Committers and/or Eclipse Membership at Large to discuss the proposed archiving of a Project. The desired outcome is to find sufficient evidence of renewed interest and resources in keeping the Project active.

## Project

<!-- Project name and termination review link from the PMI -->

Project: [{project_name}](PMI link)

## Pre-Termination Review Checklist

_To be completed by the EMO_

- [ ] PMC has been notified
- [ ] Project team has been notified via dev-list
- [ ] Project team has been notified via email

## Post-Termination Checklist

_To be completed by EMO_

- [ ] Retire committers and project leads relationships (PMI)
- [ ] Retire Mentor relationships (Database)
- [ ] Archive the project (PMI) 
- [ ] Archive the project (Database) 
- [ ] Look into associated and similar domains (relevant for security)

_To be completed by Webmaster_

Shutdown/archive:
- [ ] CI instance (https://github.com/eclipse-cbi/jiro/tree/master/instances, https://ci.eclipse.org/)
- [ ] Website
- [ ] Git repositories (Gerrit, GitHub, GitLab)
- [ ] Mailing list 
- [ ] Forum
- [ ] Bugzilla product

Your comments, feedback, and pushback are welcome.

<!-- Specification projects only 
Being this a specification project, a Spec Committee ballot to aprove the project termination must run for at least one week
- [ ] Specification Committee ballot initiated 
- [ ] Specification Committee ballot concluded successfully
-->

<!-- Quick actions will configure the state of the issue. Leave these. -->
/label ~"Termination review" ~"Review Request"
/assign @wbeaton @mdelgado624
