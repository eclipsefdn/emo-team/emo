<!--
This template is used by the EMO to track the creation of a project.

If you want to create a new Eclipse open source project start here:  https://www.eclipse.org/projects/handbook/#starting

Parts of this issue can initially be left blank.

Set the title of this issue to the name of the proposal

You can delete the comments (or not).
-->

The EMO is using this issue to track the progress of project creation. Help regarding the process can be found in the [Eclipse Foundation Project Handbook](https://www.eclipse.org/projects/handbook/#starting).

The following image shows the workflow for [Starting a Project](https://www.eclipse.org/projects/handbook/#starting) at the Eclipse Foundation.

<!--For SPEC projects
![Onboarding Process for Specification Projects](https://gitlab.eclipse.org/eclipsefdn/emo-team/emo/-/raw/main/Template%20Images/Starting_a_project_EF_spec.png)
-->
![Onboarding Process](https://gitlab.eclipse.org/eclipsefdn/emo-team/emo/-/raw/main/Template%20Images/Starting_a_project_EF.png)

We are using scoped labels to identify where a project proposal is at any time during the onboarding process. These labels are mapped to the stages shown in the previous image and will define the "state" of this ticket. If you have doubts, just ask.

We'll use the due date of this issue to provide an estimation of the next date the EMO team will checkin with the project team. Sometimes, e.g. during the project creation time, the due date will coincide with the Project Creation date.

## Project

<!-- Proposed project name and proposal link from the PMI -->

Project proposal: [{project_name}](https://projects.eclipse.org/proposals/{proposal})

Top level project: 

## Basic Information

- Licence: {licence}
<!-- Specification projects only
- Patent Licence {Patent licence}
-->

## Pre-creation Checklist

_To be completed by the EMO_

- [ ] Project scope is well-defined
- [ ] Top-level Project selected
- [ ] PMC accepts the project (PMC has replied with +1)
- [ ] Licence(s) approved
- [ ] Project name -nor similar names- has NOT been previously used by the team
- [ ] Project name trademark approved. If needed: please complete the [Trademark transfer agreement available here](https://www.eclipse.org/legal/Trademark_Transfer_Agreement.pdf) and send the completed agreement to trademarks@eclipse-foundation.org
- [ ] Project lead(s) identified
- [ ] Committers identified
- [ ] Committers have Eclipse Foundation Accounts
- [ ] Two weeks of community review completed
- [ ] The Membership team is aware of the new project and will reach out about membership opportunities (email sent to membership@eclipse.org)


<!-- Specification projects only
This being a specification project, a Spec Committee ballot to approve the project creation must run for at least one week
- [ ] Specification Technical Committe / Steering commmitte approves the project under the purview of the WG (required before starting the ballot)
- [ ] Specification Committee ballot initiated 
- [ ] Specification Committee ballot concluded successfully
-->

## Project resources to be created 
_Project Lead involvement needed_
Open source projects at the Eclipse Foundation are required to make use of certain Eclipse Foundation services. See the [Handbook](https://www.eclipse.org/projects/handbook/#project-resources-and-services) for more information on this subject.

- [ ] The project lead has confirmed the project shortname: AAAAAAAA 
- [ ] The project lead has requested a project website repository in the comments
- [ ] The project lead has confirmed the following project resources are correct in the comment's section


<!--
GitLab
- Project repository: https://gitlab.eclipse.org/eclipse/{project_name}/{project_name}  
GitHub
- Project organization: https://github.com/eclipse-{project_name}  

The [OtterDog self-service management tool](https://github.com/eclipse-csi/otterdog) will be automatically enabled to help the Project team manage their GitHub project.
- Downloads: http://download.eclipse.org/{project_name}
- Archives: http://archive.eclipse.org/{project_name}
-->
- Project mailing list: https://accounts.eclipse.org/mailing-list/{project_name}-dev
- To request a CI instance, the project team should submit a ticket through our HelpDesk, following the provided [template](url).

## Post-creation Checklist

_To be completed by the EMO_

EMO internal tasks:
- [ ] Add @webmaster and marketing teams to the issue
- [ ] Project DB entry created 
- [ ] Project lead(s) designated (DB)

Milestones:
- [ ] Project created
- [ ] Project provisioned (including at least one committer)
- [ ] Committer orientation completed (upon request)

## After creation
Once the project is created the [provisioning process](https://www.eclipse.org/projects/handbook/#starting-provisioning) starts with an email message being sent to each of the committers listed on the project proposal with instructions on how to engage in the [committer paperwork](https://www.eclipse.org/projects/handbook/#paperwork) process. 

Committers should check their emails and complete the paperwork at their earliest convenience. **No project resources are created until at least one committer is fully provisioned.**

_To be completed by the EMO_
- [ ] GitLab ticket state set to Provisioning

## After provisioning

Following provisioning, the project will be ready for initial contribution review. An initial contribution is basically the first set of project code contributions to the new Eclipse project. An initial contribution typically takes the form of an entire existing source code repository or some snapshot of an existing code base.

At this point, the project team might proceed to push their code into the requested repositories or coordinate with Webmaster and EMO to transfer existing ones. Once you've completed this process please notify EMO using this ticket.

_To be completed by the EMO_
- [ ] GitLab ticket state set to Initial Contribution

## IP-check
_To be completed by the EMO_

Once the project has provided their initial contribution:
- [ ] IP check completed
- [ ] GitLab ticket state set to Fully Operational


<!-- Quick actions will configure the state of the issue. Leave these. -->
/label ~"Project Proposal" ~"Trademark Approval Request" ~"License Approval Request" ~"EDP::CommunityReview"
<!-- Specification projects only
/label~"ballot
-->
/assign @mdelgado624
