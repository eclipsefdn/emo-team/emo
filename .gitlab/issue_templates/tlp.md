Recently, the Eclipse Foundation's Board of Directors approved the creation of the XYZ Top-Level Project. 

We'll use this issue to track all of the activities that we need to undertake to get bootstrap the new Top-Level Project.

> Meta: creating Top-Level Projects is a relatively rare occurrence, so our processes are not particularly refined (combine this with the fact that we're evolving all of our processes). Given that the nature of when and how we stand up new Top-Level Projects is evolving, we need to also evolve corresponding process and tools. We'll use our experience with the creation of this Top-Level Project to capture the process as best we can and turn it into something more refined and repeatable. If anything is confusing at any point in this process, please ask for clarification. We will move quickly; your patience is appreciated.

## EMO checklist

To create a new Top-Level Project, _the EMO_ needs to:

- [ ] Select a short name for the project
- [ ] Create a record in the Foundation DB (internal)
- [ ] Provision backend resources (e.g., LDAP)
- [ ] Assign the PMC Lead and PMC Member roles
- [ ] Create a record in the PMI
- [ ] Capture the charter in the PMI
- [ ] Create the PMC Mailing list
- [ ] Verify that PMC Lead and PMC Member roles have been propagated to the PMI
- [ ] Align existing subprojects with the new Top-Level Project
- [ ] Set up a call with the new PMC

Following our usual convention, the short name for the new Top-Level Project would be `xyz`, which would make the ID of the XYZ project `xyz.xyz` meaning that the PMI page URL would, for example, become `https://projects.eclipse.org/projects/xyz.xyz/`. 

## Subprojects

Subprojects that we need to align with the new Top-Level Project:

- tlp.sp1
- tlp.sp2

## Resources 

Please specify in this section the resource requirements of the TLP (Git repositories, website, etc.). 
(The TLP is entitled to a website)

The notion of committers on a Top-Level project is not generally meaningful in the case where a Top-Level project has none of its own resources. In the event that the Top-Level requires, say, a Git repository of it's own, we'll add committers.

<!-- Quick actions will configure the state of the issue. Leave these. -->
/label ~"TLP"
/assign @wbeaton @mdelgado624
