<!--
The EMO is using this template to track the status of Restrucring reviews.

There's help in the Eclipse Foundation Project Handbook https://www.eclipse.org/projects/handbook/

Parts of this issue can initially be left blank to be filled in during the review.
Set the title of this issue to the project's id, e.g., "technology.dash Restructuring Review"

You can delete the comments (or not).
-->

## Restructuring rationale
<!--
Provide relevant links to the restruturing review, e.g. review record, public discussions, links to messages in the public mailing lists.
-->

PMI link: [Project restructuring record](link here)

The project team is requesting a restructuring review to address the following issues:
<!--
Provide a brief explanation of the reasons you need a restructuring review together with some context.
--> 

## Expected infrastructure changes
<!--
The project team should list the expected infrastructure changes in thsi section, e.g. new project name and shortname, which repositories to move, repositories that need to be archived, committer's databases to be merged/splitted, etc.
--> 
The following infrastructure changes are requested by the project team:
- Project name
- Committers
- Project repositories
- CI instance
- Mailing list
- Other(s)

## Restructuring To-Do list
In order to implement the proposed changes, we need to complete the following:
- [ ] EMO (ED) approval
- [ ] PMC approval
- [ ] Project community informed using public channels (i.e. dev mailing list and/or forums)
- [ ] Releng team informed (add @fgurr)
- [ ] IT team informed (add @webmaster)
- [ ] Infrastructure changes implemented

<!--
If the restructuring review is related to a specification project you'll need the Specification committee approval
- [ ] Spec committee approval
--> 

/assign @mdelgado624
